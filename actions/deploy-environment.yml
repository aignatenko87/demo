# Deploy an Environment
# -------------------------------------------------
# This job enables you to stand up a consistent, disposable environment for testing your application. It deploys and 
# provisions a Parasoft service virtualization environment to the specified Continous Testing Platform endpoint.
# -------------------------------------------------

# Common variables shared with other actions
variables:
  CTP_URL: "http://ctp.server:8080/em" # Specifies the Continuous Testing Platform endpoint where the environment will be deployed
  CTP_USERNAME: "username" # Specifies a username for accessing the Continuous Testing Platform endpoint
  CTP_PASSWORD: "password" # Specifies a password for accessing the Continuous Testing Platform endpoint

deploy-environment: 
  image: alpine:latest
  before_script:
    - apk add curl jq # curl and jq are required for the script content
  variables: # deploy environment specific variables
    CTP_SYSTEM: "system" # Specifies the name of the system in Continous Testing Platform with the environment instance to provision
    CTP_ENVIRONMENT: "environment" # Specifies the name of the environment with instances to provision
    CTP_INSTANCE: "instance" # Specifies the environment instance to provision
    COPY_ENVIRONMENT: "false" # Copy the environment and assets before provisioning
    NEW_ENVIRONMENT_NAME: "" # New enviornment name when COPY_ENVIRONMENT is true
    VIRTUALIZE_SERVER_NAME: "" # Copy to a Virtualize server matching name when COPY_ENVIRONMENT is true
  script:
      # find system with system name
      - >
        system=$(curl -sS -G --user $CTP_USERNAME:$CTP_PASSWORD 
        --header "Content-Type: application/json" --request GET 
        --data-urlencode "name=$CTP_SYSTEM"
        $CTP_URL/api/v2/systems)
      - if [ "$system" == "{}" ]; then
      - echo "No system found with name $CTP_SYSTEM"
      - exit 1
      - fi
      - systemId=$(echo $system | jq -r --arg name "$CTP_SYSTEM" 'first(.systems[] | select(.name == $name)) | .id')
      # find environment with environment name
      - >
        environment=$(curl -sS -G --user $CTP_USERNAME:$CTP_PASSWORD 
        --header "Content-Type: application/json" --request GET 
        --data-urlencode "name=$CTP_ENVIRONMENT"
        $CTP_URL/api/v2/environments)
      - if [ "$environment" == "{}" ]; then
      - echo "No environment found with name $CTP_ENVIRONMENT"
      - exit 1
      - fi
      - >
        environmentId=$(echo $environment | jq -r --arg name "$CTP_ENVIRONMENT" 
        --arg id "$systemId" 'first(.environments[] | select(.name == $name and .systemId == ($id|tonumber))) | .id')
      # if copy is enabled, copy environment to server
      - if [[ "$COPY_ENVIRONMENT" == "true" ]]; then
      - >
        servers=$(curl -sS -G --user $CTP_USERNAME:$CTP_PASSWORD 
        --header "Content-Type: application/json" --request GET 
        --data-urlencode "name=$VIRTUALIZE_SERVER_NAME"
        $CTP_URL/api/v2/servers)
      - if [ "$servers" == "{}" ]; then
      - echo "No server found with name $VIRTUALIZE_SERVER_NAME"
      - exit 1
      - fi
      - serverId=$(echo $servers | jq -r --arg name "$VIRTUALIZE_SERVER_NAME" 'first(.servers[] | select(.name == $name)) | .id')
      - >
        copyResult=$(curl -sS --user $CTP_USERNAME:$CTP_PASSWORD 
        --header "Content-Type: application/json" --request POST 
        --data '{"originalEnvId": "'"$environmentId"'","serverId":"'"$serverId"'","newEnvironmentName":"'"$NEW_ENVIRONMENT_NAME"'"}' 
        $CTP_URL/api/v2/environments/copy?async=false)
      - environmentId=$(echo $copyResult | jq .environmentId)
      - fi
      # find instance with instance name
      - >
        instance=$(curl -sS -G --user $CTP_USERNAME:$CTP_PASSWORD 
        --header "Content-Type: application/json" --request GET 
        --data-urlencode "name=$CTP_INSTANCE"
        $CTP_URL/api/v2/environments/$environmentId/instances)
      - if [ "$instance" == "{}" ]; then
      - echo "No instance found with name $CTP_INSTANCE"
      - exit 1
      - fi
      - instanceId=$(echo $instance | jq -r --arg name "$CTP_INSTANCE" 'first(.instances[] | select(.name == $name)) | .id')
      # deploy environment to instance
      - >
        if [[ "$COPY_ENVIRONMENT" == "true" ]]; then
          echo "Deploying environment $NEW_ENVIRONMENT_NAME to $CTP_INSTANCE..."
        else
          echo "Deploying environment $CTP_ENVIRONMENT to $CTP_INSTANCE..."
        fi
      - >
        result=$(curl -sS --user $CTP_USERNAME:$CTP_PASSWORD 
        --header "Content-Type: application/json" --request POST 
        --data '{"environmentId": "'"$environmentId"'","instanceId":"'"$instanceId"'","abortOnFailure":false}'
        $CTP_URL/api/v2/provisions?async=false)
      - status=$(echo $result | jq -r .status)
      - > 
        if [ "$status" == "success" ]; then
          echo "Successfully provisioned $CTP_INSTANCE."
          exit 0;
        else
          echo "Provisioning failed with status $status."
          exit 1;
        fi
